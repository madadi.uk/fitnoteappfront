# Use the Node.js 16 and Alpine Linux as a base image
FROM node:16-alpine AS development

# Set environment variables
ENV NODE_ENV development

# Set the working directory inside the container
WORKDIR /app

# Install dependencies
# First, only copy the dependency-related files and install dependencies
COPY package.json ./

RUN yarn install

# Copy the rest of the application files
COPY . .

# Expose port 3000 for the application
EXPOSE 3000

# Define the command to run the application
CMD ["npm", "run", "start"]
