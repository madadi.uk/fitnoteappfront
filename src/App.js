import { Routes, Route } from "react-router-dom";
import Header from "./features/header/Header";
import PositionsPage from "./features/positions/positionsPage/PositionsPage";
import SignUpPage from "./features/signUpPage/SignUpPage";
import SignInPage from "./features/signInPage/SignInPage";
import PositionAndItsOrders from "./features/positionAndItsOrders/PositionAndItsOrders";
import ImanLoading from "./features/loading/ImanLoading";
import { useSelector, useDispatch} from "react-redux";
import {selectImanLoadingStatus} from "./features/loading/ImanLoadingSlice";
import {changeSocketStatus} from "./features/livePrice/socketStatusSlice";
import {changeSocketPrices} from "./features/livePrice/socketPricesSlice";
import {changePositionUpdate} from "./features/livePrice/socketPositionUpdateSlice";




function App() {
    ConnectToSocket()
    const imanLoadingSelector = useSelector(selectImanLoadingStatus);
  return (
    <div className="App">
        {imanLoadingSelector ? <ImanLoading/> : ''}
      <Header className="header" />
      <Routes>
        <Route path="/" element={<PositionsPage />} />
        <Route
          path="/positionAndItsOrders/:positionId"
          element={<PositionAndItsOrders />}
        />
        <Route path="/signUp" element={<SignUpPage />} />
        <Route path="/signIn" element={<SignInPage />} />
      </Routes>
    </div>
  );
}
const ws = new WebSocket("ws://live.aurigo.xyz/ws");
// const ws = new WebSocket("ws://localhost:8081/ws");
//this function connect to socket server for get live price
//call in app for use on all pages
function ConnectToSocket() {

    const dispatch = useDispatch();

    ws.onopen = function (e) {
        dispatch(changeSocketStatus({status: true}))
        console.log("Connect")
    }

    ws.onmessage = function (event) {

        let socketResponse = JSON.parse(event.data)

        if (socketResponse.Type === "symbols") {
            dispatch(changeSocketPrices({prices: socketResponse.Symbols}))
        }
        if (socketResponse.Type === "updateOrder") {
            console.log('updateOrder')
            dispatch(changePositionUpdate({
                positionId: socketResponse.PositionId,
                orderId: socketResponse.OrderId,
                update: true
            }))
        }

    };

    ws.onclose = function (e) {
        dispatch(changeSocketStatus({status: false}))
        console.log("onclose")
        // setTimeout(function() {
        //     ConnectToSocket();
        // }, 1000);

    }

    ws.onerror = function (e) {
        dispatch(changeSocketStatus({status: false}))
        console.log("onerror")

        // setTimeout(function() {
        //     ConnectToSocket();
        // }, 1000);

    }
}


export default App;
