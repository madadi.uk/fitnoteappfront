import { configureStore } from "@reduxjs/toolkit";
import positionsReducer from "../features/positions/PositionsSlice";
// import ordersReducer from "../features/orders/OrdersSlice";
import positionStateSlice from "../features/positions/PositionState";
import exitOrderStateSlice from "../features/orders/exitOrders/ExitOrdersSlice";
import entryOrderStateSlice from "../features/orders/entryOrders/EntryOrdersSlice";
import imanLoadingStatusSlice from "../features/loading/ImanLoadingSlice";
import socketStatusSlice from "../features/livePrice/socketStatusSlice";
import socketPricesSlice from "../features/livePrice/socketPricesSlice";
import SocketPositionUpdateSlice from "../features/livePrice/socketPositionUpdateSlice";
// dfgd

const store = configureStore({
  reducer: {
    positions: positionsReducer,
    // orders: ordersReducer,
    positionState: positionStateSlice,
    entryOrderSlice: entryOrderStateSlice,
    exitOrderState: exitOrderStateSlice,
    imanLoadingSlice: imanLoadingStatusSlice,
    sliceSocketStatus: socketStatusSlice,
    sliceSocketPrice: socketPricesSlice,
    sliceSocketPositionUpdate: SocketPositionUpdateSlice,
  },
});

export default store;
