import {
  Box,
  Button,
  div,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Slider,
  TextField,
  Typography,
} from "@mui/material";
import * as React from "react";
import Modal from "@mui/material/Modal";
import "./childModal.css";
import ArrowCircleLeftIcon from "@mui/icons-material/ArrowCircleLeft";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",

  backgroundColor: "background.paper",
  boxShadow: 24,
  p: 8,
};

export default function ChildModal() {
  const [open, setOpen] = React.useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    console.log({
      description: data.get("description"),
    });
  };
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const [type, setType] = React.useState("");
  const [intention, setIntention] = React.useState("");

  const handleTypeChange = (event) => {
    setType(event.target.value);
  };
  const handleIntentionChange = (event) => {
    setIntention(event.target.value);
  };

  const [value, setValue] = React.useState(30);

  const handleSliderChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleInputChange = (event) => {
    setValue(event.target.value === "" ? "" : Number(event.target.value));
  };

  const handleBlur = () => {
    if (value < 0) {
      setValue(0);
    } else if (value > 100) {
      setValue(100);
    }
  };

  return (
    <React.Fragment>
      <div className="exit-entry-button">
        <Button
          onClick={handleOpen}
          size="large"
          variant="contained"
          color="error"
          sx={{ minWidth: "45%" }}
          startIcon={<ArrowCircleRightIcon />}
        >
          Exit
        </Button>
        <Button
          onClick={handleOpen}
          size="large"
          variant="contained"
          color="success"
          sx={{ minWidth: "45%" }}
          startIcon={<ArrowCircleLeftIcon />}
        >
          Entry
        </Button>
      </div>
      <Modal
        hideBackdrop
        open={open}
        onClose={handleClose}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
        <Box
          sx={{
            ...style,
            width: 812,
            height: 604,
            borderRadius: "0.5rem",
            pb: 0,
          }}
          className="child-modal"
          component="form"
          noValidate
          onSubmit={handleSubmit}
        >
          <div className="modal-title">
            <Typography variant="h5">+ Exit Position</Typography>
          </div>

          <div className="type-intention">
            <FormControl sx={{ minWidth: 180, marginRight: 4 }}>
              <InputLabel id="demo-simple-select-label" className="type">
                TYPE
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={type}
                label="TYPE"
                onChange={handleTypeChange}
              >
                <MenuItem value={10}>Sell</MenuItem>
                <MenuItem value={20}>Buy</MenuItem>
              </Select>
            </FormControl>

            <FormControl sx={{ minWidth: 180, marginRight: 2 }}>
              <InputLabel id="demo-simple-select-label">INTENTION</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={intention}
                label="INTENTION"
                onChange={handleIntentionChange}
              >
                <MenuItem value={10}>Short Term</MenuItem>
                <MenuItem value={20}>Long Term</MenuItem>
              </Select>
            </FormControl>
          </div>

          <TextField
            id="description"
            name="description"
            label="Describe the Order"
            required
            autoFocus
            variant="outlined"
            sx={{ marginBottom: 4 }}
          />

          <div className="stop-limit">
            <TextField
              sx={{ marginBottom: 4 }}
              className="stop"
              id="outlined-number"
              label="Stop"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              sx={{ marginBottom: 4 }}
              className="limit"
              id="outlined-number"
              label="Limit"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>
          <div className="amount-total">
            <TextField
              className="amount"
              sx={{ marginBottom: 4 }}
              label="Amount"
              value={value}
              onChange={handleInputChange}
              onBlur={handleBlur}
              inputProps={{
                step: 10,
                min: 0,
                max: 100,
                type: "number",
                "aria-labelledby": "input-slider",
              }}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              className="total"
              sx={{ marginBottom: 4 }}
              id="outlined-number"
              label="Total"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>

          <Slider
            className="slider"
            value={typeof value === "number" ? value : 0}
            onChange={handleSliderChange}
            aria-labelledby="input-slider"
            sx={{ marginBottom: 4, width: "48%" }}
          />

          <div className="buttons">
            <Button
              onClick={handleClose}
              variant="contained"
              size="large"
              sx={{ minWidth: 180, minHeight: 60, marginRight: 4 }}
              onSubmit={handleSubmit}
            >
              ADD
            </Button>
            <Button
              onClick={handleClose}
              size="large"
              sx={{ minWidth: 180, minHeight: 60 }}
            >
              CANCEL
            </Button>
          </div>
        </Box>
      </Modal>
    </React.Fragment>
  );
}
