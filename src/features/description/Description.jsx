import { TextField } from "@mui/material"

const Description = () => {
  return (
      
    <TextField
            id="description"
            name="description"
            label="Describe {prop}; it should be fixed..."
            
            autoFocus
            variant="outlined"
            sx={{ marginBottom: 4 }}
          />
  )
}

export default Description