import { createSlice } from "@reduxjs/toolkit";



const socketPositionUpdateSlice = createSlice({
  name: "socketPositionUpdateSlice",
  initialState: {
    connected: false,
    prices: []
  },
  reducers: {
    changePositionUpdate  : (state, action) => {
      state.update =  action.payload.update
      state.positionId =  action.payload.positionId
      state.orderId =  action.payload.orderId
    },
  },
});

// export const selectSocketPositionUpdate = (state) => state.sliceSocketPositionUpdate.positionId;
export const selectSocketPositionUpdate = function (state) {
  return {
    update: state.sliceSocketPositionUpdate.update,
    positionId: state.sliceSocketPositionUpdate.positionId,
    orderId: state.sliceSocketPositionUpdate.orderId,
  }
}

export const { changePositionUpdate } = socketPositionUpdateSlice.actions;

export default socketPositionUpdateSlice.reducer;
