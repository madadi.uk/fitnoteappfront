import { createSlice } from "@reduxjs/toolkit";



const socketPricesSlice = createSlice({
  name: "socketPricesSlice",
  initialState: {
    connected: false,
    prices: []
  },
  reducers: {
    changeSocketPrices: (state, action) => {
      state.prices =  action.payload.prices
    },
  },
});

export const selectSocketPrices= (state) => state.sliceSocketPrice.prices;

export const {changeSocketPrices } = socketPricesSlice.actions;

export default socketPricesSlice.reducer;
