import { createSlice } from "@reduxjs/toolkit";



const socketStatusSlice = createSlice({
  name: "changeSocketStatus",
  initialState: {
    status: false
  },
  reducers: {
    changeSocketStatus: (state, action) => {
      state.status =  action.payload.status
    },
  },
});

export const selectSocketStatus = (state) => state.sliceSocketStatus.status;

export const {changeSocketStatus } = socketStatusSlice.actions;

export default socketStatusSlice.reducer;
