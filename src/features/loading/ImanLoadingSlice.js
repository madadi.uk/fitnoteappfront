import { createSlice } from "@reduxjs/toolkit";



const imanLoadingStatusSlice = createSlice({
  name: "iman_loading_show",
  initialState: { show: true },
  reducers: {
    changeImanLoadingStatus: (state, action) => {
      state.show =  action.payload.show
    },
  },
});

export const selectImanLoadingStatus = (state) => state.imanLoadingSlice.show;

export const {changeImanLoadingStatus } = imanLoadingStatusSlice.actions;

export default imanLoadingStatusSlice.reducer;
