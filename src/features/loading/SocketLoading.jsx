
import "./ImanLoading.css";
import { selectSocketStatus } from "../livePrice/socketStatusSlice";
import { useDispatch, useSelector } from "react-redux";


export default  function SocketLoading(props) {

    const socketStatusSelector = useSelector(selectSocketStatus);

    const loadingStyle = {
        position: "fixed",
        left: "10px",
        bottom: "2px",
        padding: "5px",
        borderRadius: "5px",
        color: "#fff",
        background: socketStatusSelector ? "green" : "red",
    };





    return(

        <div className="socketConnectionLoading" style={loadingStyle}>
            {
                socketStatusSelector ?
                    "Connected" :
                   "Connecting to Socket..."
            }

        </div>
    )
}