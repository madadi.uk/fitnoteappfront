import { useState, useEffect } from "react";
// Redux
import { useDispatch, useSelector } from "react-redux";

// import { orderAdded } from "../orders/OrdersSlice";
//MUI
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import { grey } from "@mui/material/colors";
import {
  Box,
  FormControl,
  InputLabel,
  LinearProgress,
  MenuItem,
  Paper,
  Select,
  Slider,
  TextField,
  Typography,
} from "@mui/material";
// Axios
import axios from "axios";
// Toast
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useParams, useLocation } from "react-router-dom";
import { changeEntryOrderState } from "../orders/entryOrders/EntryOrdersSlice";
import { changeImanLoadingStatus } from "../loading/ImanLoadingSlice";
import { selectSocketStatus } from "../livePrice/socketStatusSlice";
import { selectSocketPrices } from "../livePrice/socketPricesSlice";
// Style
// import "./addPositionModal.css";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};

export default function AddOrderModal(props) {
  // Note: In "Add Order Modal" the "type" will be sent to the backend as "side", "limit" will be sent to the backend as "price" and "amount" will be sent to the backend as "quantity".
  const [genre, setGenre] = useState("");
  const [price, setPrice] = useState(0);
  const [value, setValue] = useState(1);
  const [side, setSide] = useState("");
  const [description, setDescription] = useState("");
  const [quantity, setQuantity] = useState(0);
  const [type, setType] = useState("");
  const [intention, setIntention] = useState("");
  const [stopHere, setStopHere] = useState("");
  const [limit, setLimit] = useState("");
  const [amount, setAmount] = useState("");
  const [total, setTotal] = useState("");
  const [marketName, setMarketName] = useState("");
  const [symbolName, setSymbolName] = useState("");
  const [availableMarket, setAvailableMarket] = useState(0);
  const [availableText, setAvailableText] = useState("");
  let selectedSide = '';

  const params = useParams();


  function  getAvailable()
  {
        setAvailableText( '...')
        let coin = marketName;
        if (selectedSide === '') return;

        if (selectedSide === 'SELL') {
            coin = symbolName;
        }
        axios.get(`/v1/wallet/coin/available/${coin}`)
              .then((response) => {
                setAvailableMarket(response.data.data.free);
                setAvailableText(response.data.data.free + " " + coin + ' is available')
              });

  }


  useEffect(() => {

      setMarketName(props.position.position.market.name);
      setSymbolName(props.position.position.symbol.name);
    // axios
    //   .get(`v1/positions/get/${params.positionId}`)
    //   .then((response) => {
    //     setMarketName(response.data.data.market.name);
    //     setSymbolName(response.data.data.symbol.name);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
  }, []);

  // Progress Bar
  function LinearProgressWithLabel(props) {
    return (
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          mb: 2,
        }}
      >
        <Box sx={{ width: "40%" }}>
          <LinearProgress variant="determinate" {...props} sx={{ mr: 2 }} />
        </Box>
        {/* <Box sx={{ maxWidth: 35, display: "flex", mb: 4 }}></Box> */}
        <Box>
          <Typography variant="caption" color="text.primary">{availableText}</Typography>
        </Box>
      </Box>
    );
  }

  // type options:['LIMIT', 'MARKET', 'STOP_LOSS', 'STOP_LOSS_LIMIT', 'TAKE_PROFIT', 'TAKE_PROFIT_LIMIT', 'LIMIT_MAKER']
  const { positionId } = useParams();
  // click Add button
  const addClicked = () => {
    dispatch(changeImanLoadingStatus({ show: true }));

    axios
      .post("v1/positions/orders/store", {
        description: description,
        position_id: positionId,
        genre: genre,
        side: type,
        type: "LIMIT",
        quantity: amount,
        price: limit,
        stop_price: stopHere,
      })
      .then((res) => {
        const successStatus = ["NEW", "PARTIALLY_FILLED", "FILLED"];

        if (successStatus.includes(res.data.data.status)) {
          toast("Order added successfully");
          setDescription(res.data.data.description);
          setGenre(res.data.data.genre);
          setPrice(res.data.data.price);
          setQuantity(res.data.data.quantity);
          setSide(res.data.data.side);
          setType(res.data.data.type);
        } else {
          // console.log(res.data.data.status);
          toast(res.data.data.status + " " + res.data.data.response_message);
        }
        dispatch(changeEntryOrderState({ status: true })); //for, if store was success reload list
        dispatch(changeImanLoadingStatus({ show: false }));
      })
      .catch((err) => {
        toast.error("error adding order");
        err.response.data.errors.map((e) => console.log(e));
        dispatch(changeImanLoadingStatus({ show: false }));
      });

    handleClose();
  };

  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    getAvailable()
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  // get values from for fields
  const onGenreChanged = (e) => setGenre(e.target.value);
  const onTypeChanged = (e) => {
    e.preventDefault();
    setType(e.target.value);
    selectedSide = e.target.value;
    getAvailable()
  };
  const onIntentionChanged = (e) => {
    setIntention(e.target.value);
  };
  const onDescriptionChanged = (e) => {
    setDescription(e.target.value);
  };
  const onStopHereChanged = (e) => setStopHere(e.target.value);
  const onLimitChanged = (e) => setLimit(e.target.value);
  const onAmountChanged = (e) => setAmount(e.target.value);
  const onTotalChanged = (e) => setTotal(e.target.value);
  const onSliderChange = (e, newValue) => {
    setValue(newValue);
  };
  const onInputChange = (e) => {
    setValue(e.target.value === "" ? "" : Number(e.target.value));
  };

  const handleBlur = () => {
    if (value < 0) {
      setValue(0);
    } else if (value > 100) {
      setValue(100);
    }
  };

  //use dispach hook
  const dispatch = useDispatch();
  var pairName = props.symbolName;
  // xrpusdt;

  const socketStatusSelector = useSelector(selectSocketStatus);
  const socketPricesSelector = useSelector(selectSocketPrices);

  const styleLive = {
    fontSize: "14px",
    textAlign: "right",
    float: "right",
    display: "inline-block",
  };

  return (
    <div>
      <Button
        variant="text"
        aria-label="add new Order"
        startIcon={<AddIcon />}
        onClick={handleOpen}
        sx={{ color: grey[900] }}
        size="large"
      >
        New Order
      </Button>
      <ToastContainer />
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
        <Box
          sx={{
            ...style,
            width: 812,
            height: 604,
            borderRadius: "0.5rem",
            pb: 0,
          }}
          className="child-modal"
          component="form"
          noValidate
        >
          <div className="modal-title">
            <Typography variant="h5">
              + New Order To <b>{pairName}</b>
              <Paper
                sx={{
                  textAlign: "right",
                  float: "right",
                  display: "inline-block",
                  margin: 4,
                  padding: 2,
                  background: "whitesmoke",
                }}
              >
                <Typography variant="subtitle2">Live price:</Typography>
                <Typography variant="h6">
                  {socketStatusSelector
                    ? socketPricesSelector[pairName]
                    : "..."}
                </Typography>
              </Paper>
              {/* </span> */}
            </Typography>
          </div>
          <br />

          <div className="type-intention">
            <FormControl sx={{ minWidth: 180, marginRight: 4 }}>
              <InputLabel id="demo-simple-select-label" className="type">
                GENRE
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                onChange={onGenreChanged}
                value={genre}
                label="GENRE"
              >
                <MenuItem value="ENTRY">Entry</MenuItem>
                <MenuItem value="EXIT">Exit</MenuItem>
              </Select>
            </FormControl>
            <FormControl sx={{ minWidth: 180, marginRight: 4 }}>
              <InputLabel id="demo-simple-select-label" className="type">
                TYPE
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={type}
                label="TYPE"
                onChange={onTypeChanged}
              >
                <MenuItem value="SELL">Sell</MenuItem>
                <MenuItem value="BUY">Buy</MenuItem>
              </Select>
            </FormControl>

            <FormControl sx={{ minWidth: 180, marginRight: 2 }} disabled>
              <InputLabel id="demo-simple-select-label">INTENTION</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={intention}
                label="INTENTION"
                onChange={onIntentionChanged}
              >
                <MenuItem value="SHORT TERM">Short Term</MenuItem>
                <MenuItem value="LONG TERM">Long Term</MenuItem>
              </Select>
            </FormControl>
          </div>
          <br />
          <TextField
            id="description"
            name="description"
            label="Describe the Order"
            value={description}
            onChange={onDescriptionChanged}
            required
            autoFocus
            variant="outlined"
            sx={{ marginBottom: 4, minWidth: "100%" }}
          />

          <div className="stop-limit">
            <TextField
              sx={{ marginBottom: 4, width: "48%" }}
              className="stop"
              id="outlined-number"
              label="Stop"
              type="number"
              value={stopHere}
              onChange={onStopHereChanged}
              InputLabelProps={{
                shrink: true,
              }}

            />
            <TextField
              sx={{ marginBottom: 4, width: "48%" }}
              className="limit"
              id="outlined-number"
              label="Limit"
              type="float"
              value={limit}
              onChange={onLimitChanged}
              inputProps={{
                step: 0.1,
                min: 0,
                max: 1000000,
                type: "number",
                "aria-labelledby": "input-slider",
              }}
              InputLabelProps={{
                shrink: true,
              }}
            />


          </div>
          <div className="amount-total">
              <TextField
                  className="amount"
                  sx={{ marginBottom: 4, width: "48%" }}
                  label="Amount"
                  value={amount}
                  onChange={onAmountChanged}
                  onBlur={handleBlur}
                  inputProps={{
                      step: 0.1,
                      min: 0,
                      max: 100,
                      type: "number",
                      "aria-labelledby": "input-slider",
                  }}
                  InputLabelProps={{
                      shrink: true,
                  }}
              />
            <TextField
              className="total"
              sx={{ marginBottom: 4, width: "48%" }}
              id="outlined-number"
              label="Total"
              type="number"
              value={amount * limit}
              // onChange={onTotalChanged}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>

          <LinearProgressWithLabel
            variant="determinate"
            value={availableMarket}
            colorPrimary
            sx={{ mb: 2 }}
          />

          <div className="buttons">
            <Button
              onClick={addClicked}
              variant="contained"
              size="large"
              sx={{ minWidth: 180, minHeight: 60, marginRight: 4 }}
            >
              ADD
            </Button>
            <Button
              onClick={handleClose}
              size="large"
              sx={{ minWidth: 180, minHeight: 60 }}
            >
              CANCEL
            </Button>
          </div>
        </Box>
      </Modal>
    </div>
  );
}
