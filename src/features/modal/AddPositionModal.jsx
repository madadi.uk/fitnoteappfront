import { useEffect, useState } from "react";
// Redux
import { useDispatch } from "react-redux";
import { positionAdded } from "../positions/PositionsSlice";
import { changeState } from "../positions/PositionState";
// Axios
import axios from "axios";
// Toast
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

// MUI
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import { grey } from "@mui/material/colors";
import {
  Autocomplete,
  Box,
  Card,
  CardContent,
  FormControl,
  TextField,
  Typography,
} from "@mui/material";
// Styles
import "./addPositionModal.css";

//Function to insert some string at somewhere in another string
function insertAt(str, index, value) {
  return str.substr(0, index) + value + str.substr(index);
}

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};

export default function AddPositionModal(props) {
  //use states
  // const [totalBalance, setTotalBalance] = useState([]);
  // const [totalBalanceBtc, setTotalBalanceBtc] = useState([]);
  // const [pairs, setPairs] = useState([]);
  const [intentions, setIntentions] = useState([]);
  const [pairId, setPairId] = useState("");
  const [intentionId, setIntentionId] = useState(0);
  const [description, setDescription] = useState("");
  // const [markets, setMarkets] = useState([]);
  const [marketId, setMarketId] = useState(0);
  const [btnLoadingStatus, setBtnLoadingStatus] = useState(false);

  const [symbols, setSymbols] = useState([]);
  const [symbolId, setSymbolId] = useState(0);

  //open and close state of Modal
  const [open, setOpen] = useState(false);


  let markets = props.initial.markets
  let totalBalance = props.initial.wallet_balance.total_balance
  let totalBalanceBtc = props.initial.wallet_balance.total_balance_btc



  // get values from for fields
  // const onIntentionChanged = (e) => setIntention(e.target.value);
  const onDescriptionChanged = (e) => setDescription(e.target.value);
  // open Modal
  const handleOpen = () => {

    //when  the modal is opened set intention
    axios.get("v1/positions/intentions").then((res) => {
      var intentionList = res.data.data.map(function (item) {
        return {
          key: item.id,
          id: item.id,
          title: item.title,
        };
      });

      if (intentionList) {
        setIntentions(intentionList);
      }
    });


    setOpen(true);
  };
  //close Modal
  const handleClose = function () {
    setOpen(false);
  };
  //use dispach hook
  const dispatch = useDispatch();
  // click Add button
  const addClicked = () => {
    if (marketId === 0) {
      toast.error("Please select Market");
      return;
    }

    if (symbolId === 0) {
      toast.error("Please select Symbol");
      return;
    }

    if (intentionId === 0) {
      toast.error("Please select Intention");
      return;
    }
    setBtnLoadingStatus(true);

    axios
      .post("v1/positions/store", {
        market_id: marketId,
        symbol_id: symbolId,
        intention_id: intentionId,
        description: description,
      })
      .then((res) => {
        toast("Position added successfully");

        dispatch(changeState({ value: true })); //when position saved successfully
        dispatch(positionAdded(pairId, intentionId, description));
        setBtnLoadingStatus(false);
        handleClose();
      })
      .catch((err) => {});
  };

  // use API to get pairs
  // useEffect(() => {
  //   axios.get("v1/initial").then((res) => {
  //
  //     // setMarkets(res.data.data.markets);
  //     // setTotalBalance(res.data.data.wallet_balance.total_balance);
  //     // setTotalBalanceBtc(res.data.data.wallet_balance.total_balance_btc);
  //   });
  // }, []);


  return (
    <div>


      <div className="container" style={{textAlign: 'center', width: '100%'}}>


        total_balance:   {totalBalance ? totalBalance : 0}
        <br/>
        total_balance_btc:   {totalBalance ? totalBalanceBtc : 0}



      </div>




      <Button
        variant="text"
        aria-label="add new position"
        startIcon={<AddIcon />}
        onClick={handleOpen}
        sx={{ color: grey[900] }}
        size="large"
      >
        New Position
      </Button>
      <ToastContainer />
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="parent-modal-title"
        aria-describedby="parent-modal-description"
      >
        <div
          sx={{
            ...style,
            width: 400,
            borderRadius: "0.2rem",
          }}
        >
          <Card
            sx={{
              ...style,
              maxWidth: 345,
              borderRadius: "0.2rem",
            }}
          >
            <CardContent>
              <Typography
                gutterBottom
                variant="h5"
                component="div"
                sx={{ mb: 4 }}
              >
                + New Position
              </Typography>

              <FormControl fullWidth>
                {/* SelectPairs */}

                {/*//makret*/}
                <Autocomplete
                  getOptionLabel={(option) => option.name || ""}
                  id="select-market"
                  sx={{ width: "full" }}
                  onChange={(event, market) => {
                    setMarketId(market.id);
                    setSymbols(market.symbols);
                  }}
                  options={markets}
                  autoHighlight
                  renderOption={(props, option) => (
                    <Box
                      key={option.id}
                      component="li"
                      sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                      {...props}
                    >
                      {option.name}
                    </Box>
                  )}
                  renderInput={(params) => (
                    <div className="select-pairs">
                      <TextField
                        {...params}
                        label="Select Base"
                        inputProps={{
                          ...params.inputProps,
                          autoComplete: "new-password", // disable autocomplete and autofill
                        }}
                      />
                    </div>
                  )}
                />

                <br />
                {/*//symbol */}
                <Autocomplete
                  getOptionLabel={(option) => option.name || ""}
                  sx={{ width: "full" }}
                  onChange={(event, symbol) => {
                    setSymbolId(symbol.id);
                  }}
                  options={symbols}
                  autoHighlight
                  renderOption={(props, option) => (
                    <Box
                      key={option.id}
                      component="li"
                      sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                      {...props}
                    >
                      {option.name}
                    </Box>
                  )}
                  renderInput={(params) => (
                    <div className="select-pairs">
                      <TextField
                        {...params}
                        label="Select Your Asset"
                        inputProps={{
                          ...params.inputProps,
                          autoComplete: "new-password", // disable autocomplete and autofill
                        }}
                      />
                    </div>
                  )}
                />

                <br />

                {/* Select Intention */}
                <Autocomplete
                  getOptionLabel={(option) => option.title || ""}
                  onChange={(event, newValue) => {
                    setIntentionId(newValue.id);
                  }}
                  id="select-intentions"
                  sx={{ width: "full", mb: 4 }}
                  options={intentions}
                  autoHighlight
                  renderOption={(props, intentionOption) => (
                    <Box
                      key={intentionOption.id}
                      component="li"
                      sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                      {...props}
                    >
                      {intentionOption.title}
                    </Box>
                  )}
                  renderInput={(params) => (
                    <div className="select-intentions">
                      <TextField
                        {...params}
                        label="Select your intention"
                        inputProps={{
                          ...params.inputProps,
                          autoComplete: "new-password", // disable autocomplete and autofill
                        }}
                      />
                    </div>
                  )}
                />
                {/* End of Select Intention */}
                {/* description */}
                <TextField
                  id="description"
                  name="description"
                  label="Describe your position"
                  autoFocus
                  variant="outlined"
                  sx={{ marginBottom: 4 }}
                  value={description}
                  onChange={onDescriptionChanged}
                />
                {/* End of description */}
                <div className="buttons">
                  <Button
                    variant="contained"
                    size="small"
                    onClick={addClicked}
                    sx={{ minWidth: 140, minHeight: 40 }}
                  >
                    {btnLoadingStatus ? "Loading..." : "Add Postion"}
                  </Button>
                  <Button
                    onClick={handleClose}
                    size="small"
                    sx={{ minWidth: 140, minHeight: 40 }}
                  >
                    CANCEL
                  </Button>
                </div>
              </FormControl>
            </CardContent>
          </Card>
        </div>
      </Modal>
    </div>
  );
}
