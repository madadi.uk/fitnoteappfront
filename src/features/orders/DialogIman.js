

export default function DialogIman()
{
    return (
        <Dialog
            open={openEntryDialoge}
            onClose={handleCloseEntryOrder}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                {"Are you sure you want to delete this order?"}
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    The type of this order is {order.type}. You ordered to{" "}
                    {order.side} {order.quantity} at {order.price}. You intended
                    to trade {asset.toUpperCase()} with {base.toUpperCase()}.
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => deleteOrder(order.id)}>Yes</Button>
                <Button onClick={handleCloseEntryOrder} autoFocus>
                    No
                </Button>
            </DialogActions>
        </Dialog>
    )
}