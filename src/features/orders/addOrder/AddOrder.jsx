import { useState } from "react";
//Components
import AddOrderModal from "../../modal/AddOrderModal";
//MUI
import { Box, FormControl, MenuItem, Select } from "@mui/material";
//Style
import "./addOrder.css";

const AddOrder = (props) => {


  const [dItem, setDItem] = useState("");

  const handleChange = (event) => {
    setDItem(event.target.value);
  };
  return (
    <div className="orders-container">
      <div>
        <Box sx={{ minWidth: 200 }}>
          <FormControl fullWidth>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={dItem}
              onChange={handleChange}
            >
              <MenuItem value={10}>All orders 1</MenuItem>
              <MenuItem value={20}>All orders 2</MenuItem>
              <MenuItem value={30}>All orders 3</MenuItem>
            </Select>
          </FormControl>
        </Box>
      </div>
      <div>
        <AddOrderModal symbolName={props.symbolName} position={props.position} />
      </div>
    </div>
  );
};

export default AddOrder;
