import { createSlice } from "@reduxjs/toolkit";



const entryOrderStateSlice = createSlice({
  name: "entry_orders",
  initialState: { status: false },
  reducers: {
    changeEntryOrderState: (state, action) => {
      state.status =  action.payload.status
    },
  },
});

export const selectEntryOrderState = (state) => state.entryOrderSlice.status;

export const {changeEntryOrderState } = entryOrderStateSlice.actions;

export default entryOrderStateSlice.reducer;
