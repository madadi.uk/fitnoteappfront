import { createSlice } from "@reduxjs/toolkit";



const exitOrderStateSlice = createSlice({
  name: "exit_orders",
  initialState: { value: 0 },
  reducers: {
    changeExitOrderState: (state) => {
      state.value = true
    },
  },
});

export const selectExitOrderState = (state) => state.exitOrderState.value;

export const { changeExitOrderState } = exitOrderStateSlice.actions;

export default exitOrderStateSlice.reducer;
