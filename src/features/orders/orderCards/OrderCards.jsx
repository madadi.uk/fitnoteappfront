import { useEffect, useState } from "react";
// Redux
import { useSelector, useDispatch } from "react-redux";
import { selectEntryOrderState } from "../entryOrders/EntryOrdersSlice";
import { selectExitOrderState } from "../exitOrders/ExitOrdersSlice";
// React-router
import { useNavigate, useParams } from "react-router-dom";
//MUI
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import {
  Badge,
  Button,
  Card,
  CardContent,
  Chip,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Typography,
} from "@mui/material";
import { grey } from "@mui/material/colors";
import LinearProgress from "@mui/material/LinearProgress";
import DeleteIcon from "@mui/icons-material/Delete";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import OutputIcon from "@mui/icons-material/Output";
import InputIcon from "@mui/icons-material/Input";
// Axios
import axios from "axios";
// Toast
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
//Style
import "./orderCards.css";

import { changeEntryOrderState } from "../entryOrders/EntryOrdersSlice";
import { changeImanLoadingStatus } from "../../loading/ImanLoadingSlice";



import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import OrderBox from "../../positionAndItsOrders/OrderBox";
import ImanLoader from "../../loading/ImanLoader";
import {changePositionUpdate, selectSocketPositionUpdate} from "../../livePrice/socketPositionUpdateSlice";


export default function OrderCards(props) {
  // Opening the delete alert modal
  const [openEntryDialoge, setOpenEntryDialoge] = useState(false);
  const [openExitDialoge, setOpenExitDialoge] = useState(false);

  const handleClickOpenEntryOrder = (orderId) => {
    setOrderToDelete(orderId)
    setOpenEntryDialoge(true);
  };

  const handleClickOpenExitOrder = (orderId) => {

    setOrderToDelete(orderId)
    setOpenExitDialoge(true);
  };
  // Closdin the delete alert modal
  const handleCloseEntryOrder = () => {
    setOpenEntryDialoge(false);
  };
  const handleCloseExitOrder = () => {
      setOpenExitDialoge(false);
  };
  const [orderToDelete, setOrderToDelete] = useState(0);
  const [entryOrders, setEntryOrders] = useState([]);
  const [exitOrders, setExitOrders] = useState([]);
  const [asset, setAsset] = useState("");
  const [base, setBase] = useState("");
  const [getPosition, setGetPosition] = useState(false);

  const entryOrdersState = useSelector(selectEntryOrderState);
  const successStatus = ["NEW", "PARTIALLY_FILLED", "FILLED"];
  const notShowBtnCancel = [
    "REJECTED",
    "CANCELED",
    "FILLED",
    "EXPIRED",
    "PENDING_CANCEL",
  ];

  const OrderStatus = [
    "ALL",
    "NEW",
    "WAITING_TO_EXECUTE",
    "PARTIALLY_FILLED",
    "REJECTED",
    "CANCELED",
    "FILLED",
    "EXPIRED",
    "PENDING_CANCEL",
  ];

  const dispatch = useDispatch();

  const params = useParams();

  const deleteOrder = (orderId) => {
    dispatch(changeImanLoadingStatus({ show: true }));


    axios
      .get(`v1/positions/orders/cancel/${orderToDelete}`)
      .then((response) => {
        dispatch(changeImanLoadingStatus({ show: false }));
        toast(`order number ${orderId}${response.data.data.message}`);
        dispatch(changeEntryOrderState({ status: true })); //if cancel success reload list
      })
      .catch((err) => {
        dispatch(changeImanLoadingStatus({ show: false }));
        // toast(err.message);
      });
    handleCloseExitOrder();
  };

  const sendRequestGetPositionById = () => {
    axios
      .get(`v1/positions/get/${params.positionId}`)
      .then((response) => {
        console.log(response.data.data);
        setAsset(response.data.data.symbol.name);
        setBase(response.data.data.market.name);
        setEntryOrders(response.data.data.entry_orders);
        setExitOrders(response.data.data.exit_orders);
        setGetPosition(true)
      })
      .catch((err) => {
        console.log(err);
      });
  };

  if (entryOrdersState) {
    sendRequestGetPositionById();
    dispatch(changeEntryOrderState({ status: false })); //change state to false
  }

  useEffect(() => {
    setAsset(props.position.symbol.name);
    setBase(props.position.market.name);
    setEntryOrders(props.position.entry_orders);
    setExitOrders(props.position.exit_orders);
    setGetPosition(true)
  }, []);

  const navigate = useNavigate();
  // Progress Bar
  function LinearProgressWithLabel(props) {
    return (
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Box sx={{ width: "100%", mr: 1 }}>
          <LinearProgress variant="determinate" {...props} />
        </Box>
        <Box sx={{ maxWidth: 35, display: "flex", mb: 4 }}></Box>
      </Box>
    );
  }
  // Render Exit Orders
  const renderedExitOrders = exitOrders?.map((order) => (
    <Grid
      item
      sm={12}
      md={6}
      key={order.id}
      s="true"
      style={{ margin: "15px auto" }}
    >
      <Card className="order-card" sx={{ position: "relative" }}>
        <CardContent
          sx={{
            fontSize: "0.8rem",
            display: "flex",
            alignItems: "center",
            width: "35rem",
          }}
        >
          <div className="entry-or-exit-icon">
            <OutputIcon fontSize="large" color="error" />
          </div>

          <div className="order-card-info">
            <Container
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <div className="percent-pnl-arrow">
                <Typography sx={{ fontSize: "0.8rem" }}>
                  {(order.exec_quantity / order.quantity) * 100}%
                </Typography>
                <Typography sx={{ fontSize: "0.8rem" }}>(-%)</Typography>
                <ArrowDropDownIcon />
              </div>

              <Chip
                label={order.status}
                sx={{
                  color: "white",
                  bgcolor:
                    order.status === "FILLED"
                      ? "#4caf50"
                      : order.status === "NEW"
                      ? "#03a9f4"
                      : order.status === "CANCELED"
                      ? "#ef5350"
                      : order.status === "EXPIRED"
                      ? "#ff9800"
                      : "#ba68c8",
                }}
                size="small"
              />
            </Container>
            <Container
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Typography sx={{ fontSize: "1.1rem", fontWeight: "bold" }}>
                {order.quantity}
              </Typography>
              <Typography
                variant="subtitle"
                sx={{ textAlign: "right", color: grey[500] }}
              >
                {order.side}
              </Typography>
            </Container>
            <Container
              sx={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography variant="subtitle" color={grey[500]} sx={{ mr: 0.5 }}>
                STOP&nbsp;
              </Typography>
              <Typography variant="subtitle" color={grey[500]} sx={{ mr: 2 }}>
                ({order.stop_price ?? ''})
              </Typography>
              <Typography variant="subtitle" color={grey[500]} sx={{ mr: 0.5 }}>
                LIMIT&nbsp;
              </Typography>
              <Typography variant="subtitle" color={grey[500]} sx={{ mr: 2 }}>
                {order.price}
              </Typography>

              <IconButton
                sx={{ color: "#c62828" }}
                size="small"
                onClick={() => handleClickOpenExitOrder(order.id)}
                // onClick={handleClickOpenExitOrder}
                // {() => deleteOrder(order.id)}
                disabled={notShowBtnCancel.includes(order.status)}
              >
                <DeleteIcon />
              </IconButton>

              <Dialog
                open={openExitDialoge}
                onClose={handleCloseExitOrder}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">
                  {"Are you sure you want to delete this order?"}
                </DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    The type of this order is {order.type}. You ordered to{" "}
                    {order.side} {order.quantity} at {order.price}. You intended
                    to trade {asset.toUpperCase()} with {base.toUpperCase()}.
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={() => deleteOrder(order.id)}>Yes</Button>
                  <Button onClick={handleCloseExitOrder} autoFocus>
                    No
                  </Button>
                </DialogActions>
              </Dialog>
            </Container>
          </div>
        </CardContent>
        <LinearProgressWithLabel
          variant="determinate"
          value={(order.exec_quantity / order.quantity) * 100}
          color="error"
          sx={{ mb: 2 }}
        />
        <Box
          sx={{
            borderRadius: 1,
            background: "#f2f2f2",
            boxShadow: "20px 20px 60px #bfbebe,-20px -20px 60px #ffffff",
            height: 50,
            mb: 2,
          }}
        >
          <Typography
            sx={{ fontSize: "0.6rem", fontStyle: "italic", p: 2 }}
            color={grey[900]}
            gutterBottom
          >
            {order.description}
          </Typography>
        </Box>
      </Card>
    </Grid>
  ));

  // Render Entry Orders
  const renderedEntryOrders = entryOrders?.map((order) => (
    <Grid
      item
      sm={12}
      md={6}
      key={order.id}
      s="true"
      style={{ margin: "15px auto" }}
    >
      <Card className="order-card" sx={{ position: "relative" }}>
        <CardContent
          sx={{
            fontSize: "0.8rem",
            display: "flex",
            alignItems: "center",
            width: "35rem",
          }}
        >
          <div className="entry-or-exit-icon">
            <InputIcon fontSize="large" color="success" />
          </div>

          <div className="order-card-info">
            <Container
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <div className="percent-pnl-arrow">
                <Typography sx={{ fontSize: "0.8rem" }}>
                  {(order.exec_quantity / order.quantity) * 100}%
                </Typography>
                <Typography sx={{ fontSize: "0.8rem" }}>(-%)</Typography>
                <ArrowDropDownIcon />
              </div>

              <Chip
                label={order.status}
                sx={{
                  color: "white",
                  bgcolor:
                    order.status === "FILLED"
                      ? "#4caf50"
                      : order.status === "NEW"
                      ? "#03a9f4"
                      : order.status === "CANCELED"
                      ? "#ef5350"
                      : order.status === "EXPIRED"
                      ? "#ff9800"
                      : "#ba68c8",
                }}
                size="small"
              />
            </Container>
            <Container
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Typography sx={{ fontSize: "1.1rem", fontWeight: "bold" }}>
                {order.quantity}
              </Typography>
              <Typography
                variant="subtitle"
                sx={{ textAlign: "right", color: grey[500] }}
              >
                {order.side}
              </Typography>
            </Container>
            <Container
              sx={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography variant="subtitle" color={grey[500]} sx={{ mr: 0.5 }}>
                STOP&nbsp;
              </Typography>
              <Typography variant="subtitle" color={grey[500]} sx={{ mr: 2 }}>
                ({order.stop_pirce ?? ''})
              </Typography>
              <Typography variant="subtitle" color={grey[500]} sx={{ mr: 0.5 }}>
                LIMIT&nbsp;
              </Typography>
              <Typography variant="subtitle" color={grey[500]} sx={{ mr: 2 }}>
                {order.price}
              </Typography>

              <IconButton
                sx={{ color: "#c62828" }}
                size="small"
                onClick={() => handleClickOpenEntryOrder(order.id)}
                // onClick={handleClickOpenEntryOrder)}
                // {() => deleteOrder(order.id)}
                disabled={notShowBtnCancel.includes(order.status)}
              >
                <DeleteIcon />
              </IconButton>

              <Dialog
                open={openEntryDialoge}
                onClose={handleCloseEntryOrder}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">
                  {"Are you sure you want to delete this order?"}
                </DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    The type of this order is {order.type}. You ordered to{" "}
                    {order.side} {order.quantity} at {order.price}. You intended
                    to trade {asset.toUpperCase()} with {base.toUpperCase()}.
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={() => deleteOrder(order.id)}>Yes</Button>
                  <Button onClick={handleCloseEntryOrder} autoFocus>
                    No
                  </Button>
                </DialogActions>
              </Dialog>
            </Container>
          </div>
        </CardContent>
        <LinearProgressWithLabel
          variant="determinate"
          color="success"
          value={(order.exec_quantity / order.quantity) * 100}
          sx={{ mb: 2 }}
        />
        <Box
          sx={{
            borderRadius: 1,
            background: "#f2f2f2",
            boxShadow: "20px 20px 60px #bfbebe,-20px -20px 60px #ffffff",
            height: 60,
            mb: 2,
          }}
        >
          <Typography
            sx={{ fontSize: "0.6rem", fontStyle: "italic", p: 2 }}
            color={grey[900]}
            gutterBottom
          >
           Description:  {order.description}
            <br/>
            Exchange Message: {order.response_message}
            <br/>
              Time:  {order.created_at}
          </Typography>
        </Box>
      </Card>
    </Grid>
  ));


  const [filterValue, setFilterValue] = useState([])

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setFilterValue(
        // On autofill we get a stringified value.
        typeof value === 'string' ? value.split(',') : value,
    );

  };

  const handleFilter = (e) => {

    setGetPosition(false)

    let statusQuery = filterValue.join(',')
    if (filterValue.includes("ALL")) {
       statusQuery = ""
    }
    // dispatch(changeImanLoadingStatus({ show: true }));
    axios.get(`v1/positions/orders/entry-exit-of-position/${params.positionId}?status=` + statusQuery)
        .then((res) => {
          setEntryOrders(res.data.data.entry);
          setExitOrders(res.data.data.exit);
          setGetPosition(true)
          dispatch(changeImanLoadingStatus({ show: false }));
        }).catch((err) => {
      dispatch(changeImanLoadingStatus({ show: false }));
        });
  }



  const selectSocketPositionUpdateSelector = useSelector(selectSocketPositionUpdate);
  const [counterUpdatePosition, setCounterUpdatePosition] = useState(0);
  useEffect(() => {

    if (selectSocketPositionUpdateSelector.update) {
      if (selectSocketPositionUpdateSelector.positionId !== props.position.id ) {
        return ;
      }

      if (counterUpdatePosition === 0) {
        setCounterUpdatePosition(1);
        axios.get(`v1/positions/get/${params.positionId}`)
            .then((res) => {
              dispatch(changePositionUpdate({
                positionId: 0,
                orderId: 0,
                update: false
              }))

              var snd = new Audio('/alert.wav');
              snd.play();
              console.log("selectSocketPositionUpdateSelector")
              toast.success("🦄 position Update!", {  position: "bottom-center", theme: "dark", autoClose: 5000 })
              setGetPosition(false)
              setEntryOrders(res.data.data.entry_orders);
              setExitOrders(res.data.data.exit_orders);
              setGetPosition(true)
              dispatch(changeImanLoadingStatus({ show: false }));
              setCounterUpdatePosition(0);
            }).catch((err) => {
              dispatch(changeImanLoadingStatus({ show: false }));
        });

      }
    }

  }, [selectSocketPositionUpdateSelector.update])


  return (
    <>
      {sendRequestGetPositionById ? (
        <Box className="order-cards">
          <Grid
            container
            spacing={{ xs: 3, md: 3 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >

            <Grid item container md={12}>
              <Grid item md={2}>

              </Grid>
              <Grid item md={6} >

                <FormControl sx={{ m: 1, width: 300 }}>
                  <InputLabel id="demo-multiple-name-label">Status</InputLabel>
                  <Select
                      multiple
                      onChange={handleChange}
                      value={filterValue}
                      labelId="demo-multiple-name-label"
                      id="demo-multiple-name"
                  >
                    {OrderStatus.map((name) => (
                        <MenuItem
                            key={name}
                            value={name}

                        >
                          {name}
                        </MenuItem>
                    ))}
                  </Select>
                </FormControl>

                <Button
                    onClick={handleFilter}
                    variant="contained"
                    size="small"
                    sx={{ minWidth: 180, minHeight: 53, marginRight: 4, marginTop: 1 }}
                >
                  Filter
                </Button>



              </Grid>
              <Grid item md={2}>

              </Grid>
              </Grid>
              <Grid md={1}></Grid>



            {/*order boxs*/}
            {
              getPosition?

                  <Grid item container md={12}>
                    <Grid item md={6}>
                      <Chip
                          icon={<InputIcon />}
                          label="Entry Orders"
                          color="success"
                          sx={{ ml: 17, p: 1, mb: 2, borderRadius: 1 }}
                      />

                      {renderedEntryOrders}
                    </Grid>

                    <Grid item md={6}>
                      <Chip
                          icon={<OutputIcon />}
                          label="Exit Orders"
                          color="error"
                          sx={{ ml: 17, p: 1, mb: 2, borderRadius: 1 }}
                      />

                      {renderedExitOrders}
                    </Grid>
                  </Grid>



                :  ''
            }



              <Grid item md={1}></Grid>

          </Grid>
          <ToastContainer />
        </Box>
      ) : (
        "loading..."
      )}
    </>
  );
}
