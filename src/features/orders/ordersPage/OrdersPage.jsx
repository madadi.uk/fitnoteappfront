import { useEffect, useState } from "react";
//Components
import PositionShowCase from "../../positionShowCase/PositionShowCase";
import PositionData from "../../positionData/PositionData";
import PositionCharts from "../../positions/positionCharts/PositionCharts";
import AddOrder from "../addOrder/AddOrder";
import OrderCards from "../orderCards/OrderCards";
//Style
import "./ordersPage.css";

function OrdersPage() {
  return (
    <div className="orders-page">
      <PositionShowCase />
      <PositionData />
      <PositionCharts />
      <AddOrder />
      <OrderCards />
    </div>
  );
}

export default OrdersPage;
