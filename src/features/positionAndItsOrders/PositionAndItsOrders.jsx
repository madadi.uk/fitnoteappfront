// Redux
import { useDispatch, useSelector } from "react-redux";
import { selectAllPositions } from "../positions/PositionsSlice";
// Router
import { useParams, useNavigate } from "react-router-dom";
//Components
import PositionShowCase from "../positionShowCase/PositionShowCase";
import PositionData from "../positionData/PositionData";
import PositionCharts from "../positions/positionCharts/PositionCharts";
import AddOrder from "../orders/addOrder/AddOrder";
import OrderCards from "../orders/orderCards/OrderCards";
// Axios
import axios from "axios";
//Style
import "./positionAndItsOrders.css";
import { Paper, Typography } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { grey } from "@mui/material/colors";
import { useEffect, useState } from "react";
import { changeImanLoadingStatus } from "../loading/ImanLoadingSlice";
// Components
import {
  changeSocketStatus,
  selectSocketStatus,
} from "../livePrice/socketStatusSlice";
import {
  changeSocketPrices, selectPositionUpdate,
  selectSocketPrices,
} from "../livePrice/socketPricesSlice";


import {changePositionUpdate, selectSocketPositionUpdate} from "../livePrice/socketPositionUpdateSlice";
import { selectEntryOrderState } from "../orders/entryOrders/EntryOrdersSlice";
import SocketLoading from "../loading/SocketLoading";


//Function to insert some string at somewhere in another string
function insertAt(str, index, value) {
  return str.substr(0, index) + value + str.substr(index);
}
function PositionAndItsOrders() {

  //Select special state
  const [sendRequest, setSendRequest] = useState(false);
  const [position, setPosition] = useState({});
  const [symbolName, setSymbolName] = useState([]);
  const params = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  // console.log(params);
  const socketStatusSelector = useSelector(selectSocketStatus);
  const socketPricesSelector = useSelector(selectSocketPrices);
  const selectSocketPositionUpdateSelector = useSelector(selectSocketPositionUpdate);
  const [counterUpdatePosition, setCounterUpdatePosition] = useState(0);

  let sendRequestFirst = false
  useEffect(() => {
    if (sendRequestFirst === false) {
      sendRequestFirst = true;
      axios.get(`v1/positions/get/${params.positionId}`).then((response) => {
        setPosition(response.data.data);
        dispatch(changeImanLoadingStatus({ show: false }));
        setSendRequest(true);
        setSymbolName(
            response.data.data.symbol.name + response.data.data.market.name
        );
      });
    }

  }, [setSendRequest]);

  return (
    <>
      {sendRequest ? (
        <div className="orders-page">
          <Paper
            className="container"
            sx={{
              color: "white",
              m: 0,
              width: "full",
              paddingX: 8,
              paddingY: 6,
              height: 100,
              backgroundImage: "linear-gradient(#6051ce, #363062)",
            }}
          >
            <div className="backIcon-and-date">
              <Typography>
                <ArrowBackIcon
                  onClick={() => navigate(-1)}
                  clickable
                  sx={{ cursor: "pointer" }}
                />
              </Typography>
              <Typography>{position.created_at}</Typography>
            </div>
            <div className="pair-and-type">
              <Typography variant="h5">
                {position.symbol.name.toUpperCase()}/
                {position.market.name.toUpperCase()}
              </Typography>
              <Typography
                sx={{ width: 100, p: 0.8 }}
                color={grey[900]}
                bgcolor={grey[50]}
              >
                {position.intention.title}
              </Typography>
            </div>
            <div className="description-and-info">
              <Typography variant="body2">{position.description}</Typography>
              <Typography className="info" variant="body3">
                (- %)
                <ArrowDropDownIcon />
                <Typography>
                  {socketStatusSelector
                    ? socketPricesSelector[symbolName]
                    : "..."}
                </Typography>
                <Typography variant="body3">
                  {" "}
                  &nbsp;{position.market.name.toUpperCase()}
                </Typography>
              </Typography>
            </div>
          </Paper>
          <PositionData position={{position}} sendRequest={{sendRequest}}/>

          <AddOrder symbolName={symbolName}  position={{position}} />
          <OrderCards position={position} />
          <SocketLoading/>
        </div>
      ) : (
        "loading..."
      )}
    </>
  );
}

export default PositionAndItsOrders;
