import { useEffect, useState } from "react";
// Redux
import { useDispatch, useSelector } from "react-redux";
// MUI
import { Typography } from "@mui/material";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import CircleIcon from "@mui/icons-material/Circle";
// Router
import { useParams } from "react-router-dom";
// Components
import { changeImanLoadingStatus } from "../loading/ImanLoadingSlice";
// Axios
import axios from "axios";
// Style
import "./positionData.css";

const PositionData = (props) => {
  const [sendRequest, setSendRequest] = useState(false);
  const [positions, setPositions] = useState([]);
  const params = useParams();
  const dispatch = useDispatch();
  // console.log(params);


  useEffect(() => {

    // console.log(props.position)
    setPositions(props.position.position);
    setSendRequest(true);

    // axios.get(`v1/positions/get/${params.positionId}`).then((response) => {
    //   console.log(props.position.position)
    //   console.log(props.sendRequest.sendRequest)
    //   console.log(response.data.data)
    //   setPositions(response.data.data);
    //   dispatch(changeImanLoadingStatus({ show: false }));
    //   setSendRequest(true);
    // });
  }, [setSendRequest]);


  
  return (
    <>
      {sendRequest ? (
        <div className="positionData-container">
          <div className="capital">
            <Typography
              sx={{
                width: "10rem",
                textAlign: "center",
              }}
              variant="subtitle2"
            >
              Capital
            </Typography>
            <Typography
              sx={{
                width: "10rem",
                textAlign: "center",
              }}
              variant="h6"
            >
              {positions.percentage_of_filled_entry_capital.toFixed(2)}
            </Typography>
          </div>
          <div className="capital">
            <Typography
              sx={{
                width: "10rem",

                borderLeft: "thin solid #e4e3e3",
                textAlign: "center",
              }}
              variant="subtitle2"
            >
              Risk
            </Typography>
            <Typography
              sx={{
                width: "10rem",

                borderLeft: "thin solid #e4e3e3",
                textAlign: "center",
              }}
              variant="h6"
            >
              --
            </Typography>
          </div>
          <div className="capital">
            <Typography
              sx={{
                width: "10rem",

                borderLeft: "thin solid #e4e3e3",
                textAlign: "center",
              }}
              variant="subtitle2"
            >
              DAILY PNL
            </Typography>
            <Typography
              sx={{
                width: "10rem",

                borderLeft: "thin solid #e4e3e3",
                textAlign: "center",
              }}
              variant="h6"
            >
              {/*{positions.daily_pnl.arrow === "UP" ? (*/}
                <ArrowDropUpIcon sx={{ color: "green" }} />
              {/*) : positions.daily_pnl.arrow === "DOWN" ? (*/}
                <ArrowDropDownIcon sx={{ color: "red" }} />
              ) : (
                <CircleIcon fontSize="small" />
              )}
              {/*{positions.daily_pnl.percentage} %*/}
            </Typography>
          </div>
          <div className="capital">
            <Typography
              sx={{
                width: "10rem",

                borderLeft: "thin solid #e4e3e3",
                textAlign: "center",
              }}
              variant="subtitle2"
            >
              Average of Entry
              by_exec_quantity
            </Typography>
            <Typography
              sx={{
                width: "10rem",

                borderLeft: "thin solid #e4e3e3",
                textAlign: "center",
              }}
              variant="h6"
            >
              {positions.avg_of_entry_by_exec_quantity.toFixed(2)}
            </Typography>
          </div>

          <div className="capital">
            <Typography
                sx={{
                  width: "10rem",

                  borderLeft: "thin solid #e4e3e3",
                  textAlign: "center",
                }}
                variant="subtitle2"
            >
              by_quantity
            </Typography>
            <Typography
                sx={{
                  width: "10rem",

                  borderLeft: "thin solid #e4e3e3",
                  textAlign: "center",
                }}
                variant="h6"
            >
              {positions.avg_of_entry_by_quantity.toFixed(2)}
            </Typography>
          </div>
          <div className="capital">
            <Typography
              sx={{
                width: "10rem",

                borderLeft: "thin solid #e4e3e3",
                textAlign: "center",
              }}
              variant="subtitle2"
            >
              Average of TP
            </Typography>
            <Typography
              sx={{
                width: "10rem",

                borderLeft: "thin solid #e4e3e3",
                textAlign: "center",
              }}
              variant="h6"
            >
              --
            </Typography>
          </div>
          <div className="capital">
            <Typography
              sx={{
                width: "10rem",

                borderLeft: "thin solid #e4e3e3",
                textAlign: "center",
              }}
              variant="subtitle2"
            >
              Average of SL
            </Typography>
            <Typography
              sx={{
                width: "10rem",

                borderLeft: "thin solid #e4e3e3",
                textAlign: "center",
              }}
              variant="h6"
            >
              --
            </Typography>
          </div>
        </div>
      ) : (
        "loading..."
      )}
    </>
  );
};

export default PositionData;
