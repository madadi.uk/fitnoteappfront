// Redux
import { useSelector } from "react-redux";
import { selectAllPositions } from "../positions/PositionsSlice";
// MUI
import { Paper, Typography } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { grey } from "@mui/material/colors";
// Style
import "./positionShowCase.css";

function PositionShowCase() {
  //Select special state
  const positions = useSelector(selectAllPositions);
  return (
    <Paper
      className="container"
      sx={{
        color: "white",
        m: 0,
        width: "full",
        paddingX: 8,
        paddingY: 6,
        height: 100,
        backgroundImage: "linear-gradient(#6051ce, #363062)",
      }}
    >
      <div className="backIcon-and-date">
        <Typography>
          <ArrowBackIcon />
        </Typography>
        <Typography>4 July 2020 -23:45</Typography>
      </div>
      <div className="pair-and-type">
        <Typography variant="h5">BTC/USDT</Typography>
        <Typography
          sx={{ width: 100, p: 0.8 }}
          color={grey[900]}
          bgcolor={grey[50]}
        >
          LONG TERM
        </Typography>
      </div>
      <div className="description-and-info">
        <Typography variant="body2">
          This is a Winning Position that Hossein Created{" "}
        </Typography>
        <Typography className="info" variant="body3">
          (4.5%)
          <ArrowDropDownIcon />
          <Typography variant="h5">3.54765437</Typography>
          <Typography variant="body3"> &nbsp;USDT</Typography>
        </Typography>
      </div>
    </Paper>
  );
}

export default PositionShowCase;
