import { createSlice } from "@reduxjs/toolkit";
import { sub } from "date-fns";



const positionStateSlice = createSlice({
  name: "position_state",
  initialState: { value: 0 },
  reducers: {
    changeState: (state, action) => {
      state.value = action.payload.value
    },
  },
});


export const selectPositionState = (state) => state.positionState.value;

// Export positionAdded reducer
export const { changeState } = positionStateSlice.actions;

export default positionStateSlice.reducer;
