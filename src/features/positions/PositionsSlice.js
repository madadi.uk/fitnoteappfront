import { createSlice, nanoid } from "@reduxjs/toolkit";
import { sub } from "date-fns";

const initialState = [];

const positionsSlice = createSlice({
  name: "positions",
  initialState,
  reducers: {
    positionAdded: {
      reducer(state, action) {
        state.push(action.payload);
      },
      prepare(pair, intention, description) {
        return {
          payload: {
            id: nanoid(),
            date: new Date().toISOString(),
            pair,
            intention,
            description,
          },
        };
      },
    },
  },
});

// Export selectAllPositions
export const selectAllPositions = (state) => state.positions;
// Export positionAdded reducer
export const { positionAdded } = positionsSlice.actions;

export default positionsSlice.reducer;
