// Components
import ChartOne from "./ChartOne";
import ChartTwo from "./ChartTwo";
import Info from "./Info";
//Styles
import "./charts.css";

const Charts = (props) => {

  return (
    <div className="charts-container">
      <Info balance={props.initial.wallet_balance}/>
      <ChartOne />
      <ChartTwo initial={props.initial} />
    </div>
  );
};

export default Charts;
