import React from "react";
import { useState, useEffect } from "react";
// MUI
import { Box, Typography } from "@mui/material";
import { grey } from "@mui/material/colors";
// Axios
import axios from "axios";
// Style
import "./info.css";

const PNL = 54.5;

function Info(props) {

  // console.log(props)
  let balance = props.balance
  // const [sendRequest, setSendRequest] = useState(false);
  // const [balance, setBalance] = useState('');
  // const params = useParams();
  // const dispatch = useDispatch();
  // // console.log(params);

  // useEffect(() => {
  //   axios.get('v1/wallet/balance').then((response) => {
  //     setBalance(response.data.data);
  //     console.log(response.data.data)
  //     // console.log(props)
  //   });
  // }, [setBalance]);



  return (
    <div>
      <Box className="estimated-balance">
        <Typography className="estimated-balance-title" variant="h6">
          Estimated Balance:
        </Typography>
        <Typography
          className="estimated-balance-number"
          variant="h6"
          color={grey[800]}
        >
          {balance.total_balance}
        </Typography>
      </Box>
      <Box className="pnl">
        <Typography className="pnl-title" variant="h6">
          PNL:
        </Typography>
        <Typography className="pnl-number" variant="h6" color={grey[800]}>
          ${PNL}
        </Typography>
      </Box>
    </div>
  );
}

export default Info;
