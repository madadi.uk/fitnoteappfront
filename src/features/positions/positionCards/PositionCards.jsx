import { useEffect, useState } from "react";
// Redux
import { changeState, selectPositionState } from "../PositionState";
// React-router
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
// MUI
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import {
  Card,
  CardContent,
  Icon,
  Paper,
  SvgIcon,
  Typography,
} from "@mui/material";
import { grey } from "@mui/material/colors";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import { styled } from "@mui/material/styles";
// Axios
import axios from "axios";
// Styles
import "./positionCards.css";
import { changeImanLoadingStatus } from "../../loading/ImanLoadingSlice";

//Function to insert some string at somewhere in another string
function insertAt(str, index, value) {
  return str.substr(0, index) + value + str.substr(index);
}
export default function PositionCards(props) {
  const positionsState = useSelector(selectPositionState);
  const [positions, setPositions] = useState([]);
  const dispatch = useDispatch();

  function sendRequestGetPositions() {
    axios
      .get("v1/positions/list")
      .then((response) => {

        setPositions(response.data.data);
        dispatch(changeImanLoadingStatus({ show: false }));
      })
      .catch((err) => {
        console.log(err);
      });
  }

  if (positionsState) {
    sendRequestGetPositions();
    dispatch(changeState({ value: false }));
  }

  useEffect(() => {
    setPositions(props.initial.positions.list)
  }, [setPositions]);

  const navigate = useNavigate();
  // Progress Bar
  function LinearProgressWithLabel(props) {
    return (
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Box sx={{ width: "100%", mr: 1 }}>
          <LinearProgress variant="determinate" {...props} />
        </Box>
        <Box sx={{ maxWidth: 35, display: "flex", mr: 4 }}>
          <Typography variant="subtitle">
            {`${Math.round(props.value)}%`}
          </Typography>
          <Typography variant="subtitle">&nbsp;filled</Typography>
        </Box>
      </Box>
    );
  }
  // Render positions
  const renderedPositions = positions.map((position) => (
    <Grid item sm={12} md={6} key={position.id} s="true">
      <Card
        className="position-card"
        onClick={() => {
          dispatch(changeImanLoadingStatus({ show: true }));
          navigate(`/positionAndItsOrders/${position.id}`);
        }}
      >
        <CardContent sx={{ fontSize: "0.8rem", color: grey[200] }}>
          <Typography
            sx={{ textAlign: "right", fontSize: "0.8rem" }}
            color={grey[500]}
            gutterBottom
          >
            <Icon>
              <img src={position.symbol.icon} style={{width: "20px", height: "20px"}}/>
            </Icon>
            {position.time_past}
          </Typography>
          <Typography
            variant="h6"
            component="div"
            color={grey[50]}
            gutterBottom
          >
            {position.symbol.name.toUpperCase()}/
            {position.market.name.toUpperCase()}
          </Typography>
          {/* <LinearProgressWithLabel
            variant="determinate"
            color="primary"
            value={40}
          /> */}
          <Paper sx={{ my: 1, width: 120, textAlign: "center" }}>
            <Typography
              color={grey[900]}
              variant="subtitle2"
              sx={{ p: 1 }}
              gutterBottom
            >
              {position.intention.title}
            </Typography>
          </Paper>

          <Box
            sx={{
              borderRadius: 1,
              background: "#5D4FC7",
              boxShadow:
                "inset 20px 20px 60px #4f43a9, inset -20px -20px 60px #6b5be5",
              height: 100,
              mb: 2,
            }}
          >
            <Typography
              sx={{ fontSize: "0.8rem", fontStyle: "italic", p: 2 }}
              color={grey[200]}
              gutterBottom
            >
              {position.description}
            </Typography>
          </Box>
          <div className="Order-info-container">


            <div className="average-of-entry">


              percentage_of_filled_entry_capital:   {position.percentage_of_filled_entry_capital}


            </div>


            <div className="average-of-entry">


              percentage_of_filled_exit_capital:   {position.percentage_of_filled_exit_capital}


            </div>


            <div className="average-of-entry">


              percentage_of_total_capital :   {position.percentage_of_total_capital}


            </div>


            <div className="average-of-entry">
              <Typography variant="subtitle">Entry:</Typography>
              <Typography
                sx={{ fontSize: "0.8rem", fontWeight: "450" }}
                color={grey[200]}
                gutterBottom
              >
                {position.avg_of_entry}
              </Typography>
            </div>

            <div className="percentage-of-non-filled-capital">
              <Typography variant="subtitle">Capital:</Typography>
              <Typography
                sx={{ fontSize: "0.8rem", fontWeight: "450" }}
                color={grey[200]}
                gutterBottom
              >
                {position.percentage_of_none_filled_capital} %
              </Typography>
            </div>
            {/* <div className="percentage-of-filled-capital">
              <Typography variant="subtitle">
                Percentage of filled capital:
              </Typography>
              <Typography
                sx={{ fontSize: "0.8rem", fontWeight: "450" }}
                color={grey[200]}
                gutterBottom
              >
                {position.percentage_of_filled_capital}
              </Typography>
            </div> */}
            <div className="percentage-of-weekly-pnl">
              <Typography variant="subtitle">Weekly PNL:</Typography>
              <Typography
                sx={{ fontSize: "0.8rem", fontWeight: "450" }}
                color={grey[200]}
                gutterBottom
              >
                {/*{position.weekly_pnl.percentage  0} %*/}
              </Typography>
            </div>
          </div>
        </CardContent>
      </Card>
    </Grid>
  ));
  return (
    <Box className="position-cards">
      <Grid
        container
        spacing={{ xs: 3, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}
      >
        {renderedPositions}
      </Grid>
    </Box>
  );
}
