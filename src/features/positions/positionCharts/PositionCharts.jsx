import React from "react";
import "./positionCharts.css";
import ChartOne from "../charts/ChartOne";
import ChartTwo from "../charts/ChartTwo";
function PositionCharts() {
  return (
    <div className="position-charts-container">
      <ChartOne />
      <ChartTwo />
    </div>
  );
}

export default PositionCharts;
