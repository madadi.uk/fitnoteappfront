import { useState } from "react";
// MUI
import { Box, FormControl, MenuItem, Select } from "@mui/material";
//Components
import AddPositionModal from "../../modal/AddPositionModal";
//Styles
import "./positionsHandles.css";

const PositionsHandles = ( props ) => {
  const [dItem, setDItem] = useState("");

  const handleChange = (event) => {
    setDItem(event.target.value);
  };
  return (
    <div className="positions-container">
      <div>
        <Box sx={{ minWidth: 200 }}>
          <FormControl fullWidth>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={dItem}
              onChange={handleChange}
            >
              <MenuItem value={10}>All Positions 1</MenuItem>
              <MenuItem value={20}>All Positions 2</MenuItem>
              <MenuItem value={30}>All Positions 3</MenuItem>
            </Select>
          </FormControl>
        </Box>
      </div>
      <div>
        <AddPositionModal initial={props.initial} />
      </div>
    </div>
  );
};

export default PositionsHandles;
