//Components
import Charts from "../charts/Charts";
import PositionCards from "../positionCards/PositionCards";
import PositionsHandles from "../positionsHandles/PositionsHandles";
//Style
import "./positionsPage.css";
import SocketLoading from "../../loading/SocketLoading";
import { useState, useEffect } from "react";
import axios from "axios";
import {changeImanLoadingStatus} from "../../loading/ImanLoadingSlice";
import { useSelector, useDispatch } from "react-redux";

const PositionsPage = () => {

    const [initial, setInitial] = useState([]);
    const [data, setDate] = useState(false);
    const dispatch = useDispatch();
    let first = true;
    useEffect(() => {
        dispatch(changeImanLoadingStatus({ show: true }));
     if (first) {
         first = false;
         axios.get('v1/initial').then((response) => {
             setInitial(response.data.data);
             console.log(response)
             setDate(true);

             dispatch(changeImanLoadingStatus({ show: false }));
         });
     }

    }, [setInitial]);


  return (
   <>
       {
           data?
               <div>
                   <div className="container">
                       <Charts className="charts" initial={initial} />
                       <PositionsHandles initial={initial} className="positions-handles" />
                   </div>
                   <PositionCards initial={initial}  />
                   <SocketLoading/>
               </div>
               : ""
       }
   </>
  );
};
export default PositionsPage;
