import { useState } from "react";
// MUI
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

const SelectIntention = () => {
  const [intention, setIntention] = useState("");

  const handleIntentionChange = (event) => {
    setIntention(event.target.value);
  };
  return (
    
      <FormControl sx={{ width: "full" , marginBottom:4 }}>
        <InputLabel id="demo-simple-select-label">Intention</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={intention}
          label="Intention"
          onChange={handleIntentionChange}
        >
          <MenuItem value={10}>Short Term</MenuItem>
          <MenuItem value={20}>Long Term</MenuItem>
        </Select>
      </FormControl>
  );
};

export default SelectIntention;
