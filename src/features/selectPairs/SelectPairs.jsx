// Redux
import { useSelector } from "react-redux";
import { selectAllPositions } from "../positions/PositionsSlice";
// MUI
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
// Style
import "./selectPairs.css";

export default function SelectPairs() {
  // Select special state
  const positions = useSelector(selectAllPositions);
  return (
    <Autocomplete
      id="select-pairs"
      sx={{ width: "full" }}
      options={pairs}
      autoHighlight
      getOptionLabel={(option) => option.label}
      renderOption={(props, option) => (
        <Box
          component="li"
          sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
          {...props}
        >
          <img
            loading="lazy"
            width="20"
            src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
            srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
            alt=""
          />
          {option.label} ({option.code}) +{option.phone}
        </Box>
      )}
      renderInput={(params) => (
        <div className="select-pairs">
          <TextField
            className="select-pairs"
            {...params}
            label="Select your pair"
            inputProps={{
              ...params.inputProps,
              autoComplete: "new-password", // disable autocomplete and autofill
            }}
          />
        </div>
      )}
    />
  );
}

//Pairs
const pairs = [
  { code: "AD", label: "BTC", phone: "376" },
  {
    code: "AE",
    label: "LTC",
    phone: "971",
  },
  { code: "AF", label: "ETH", phone: "93" },
  {
    code: "AG",
    label: "BNB",
    phone: "1-268",
  },
];
