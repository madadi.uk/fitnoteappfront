import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
//Router
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
//Store
import store from "./app/store";
import axios from "axios";
import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";
import {isStaging} from "./utils/DefaultValue";
import {  toast } from "react-toastify";

axios.defaults.baseURL = "http://back.aurigo.xyz/api/";
// axios.defaults.baseURL = "http://localhost:8073/api/";


axios.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    if (error.response.data.message) {
        toast.error(error.response.data.message)
    }

    if (error.response.data.errors.length > 0) {
        error.response.data.errors.forEach((element) => {
            console.log(element[0])
            toast.warn(element[0])
        });
    }

    return Promise.reject(error);
});

// Sentry.init({
//     dsn: "https://66e59a9310944f67ae4849b18fc16ad5@o1294890.ingest.sentry.io/6535392",
//     integrations: [new BrowserTracing()],
//     autoSessionTracking: true,
//     tracesSampleRate: 1.0,
//     environment: isStaging() ? "staging" : "production",
// });

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </React.StrictMode>
);
