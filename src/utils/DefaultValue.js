export function isStaging() {
    const hostUrl = window.location.hostname;
    return hostUrl.indexOf("app-staging") !== -1 || hostUrl === "localhost";
}
